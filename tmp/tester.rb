
hashed_parms = {}
last_hashed = nil
ARGV.each{|a|
  if a =~ /^--/
    last_hashed = a[2..-1]
  elsif last_hashed
    hashed_parms[last_hashed] = a
    last_hashed = nil
  end
}

hashed_parms.keys.each{|k|
  pair = [k,hashed_parms[k]]
  puts "#{pair.join(',')}"
}
