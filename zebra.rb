require 'rubygems'

require 'em-websocket'

require 'json'



myhost = "0.0.0.0"

myport = 443

token = "MdfGpTspPEqsqUw75R39"



conns = []

idsByConn = Hash.new

connsByName = Hash.new

lastPings = {}


# Heartbeat to clean up old sockets
HEARTBEAT_INTERVAL = 30
HEARTBEAT_THRESHOLD = 70

Thread.new {

  loop {
    sleep(HEARTBEAT_INTERVAL)
    lastPings.each{|ws,p|
      if (Time.now.to_i - p) >= HEARTBEAT_THRESHOLD
        id = idsByConn[ws]
        puts "cleanup of #{id}"
        ws.close_connection
        conns.delete(ws)
        idsByConn.delete(ws)
        connsByName.delete(id)
        puts "cleanup done"
      end
    }
  }

}


#Connection to ItemPath
Thread.new {

  server = TCPServer.open(2000)
  loop {
    client = server.accept
    Thread.new {
      req = client.gets.strip
      req_js = JSON.parse(req)
      
      # Grab data from ItemPath JSON array
      ws_id = req_js["socket_id"]
      pr_s = req_js["zpl"]
      poll = req_js["poll"]
      tok = req_js["token"]
      
      # Get connected printers and send array to ItemPath
      if tok == token
        if poll
          polling = 
            connsByName.keys #.map{|k|
#              if connsByName[k].ping
#                k
#              else
#                nil
#              end
#            }.compact
          client.print (polling.empty? ? "nil" : polling.join(","))
        # Return status of print job to ItemPath
        elsif pr_s
          if connsByName[ws_id]
            begin
              connsByName[ws_id].send_binary pr_s
              client.print "OK"
            rescue WebSocketError => e
              client.print "error writing to socket : #{e.message}"
            rescue StandardError => e
              client.print "error : #{e.message}"
            end
          else
            client.print "No web socket open for #{ws_id}"
          end
        else
          client.print ""
        end
      else
        client.print ""
      end
      client.close
    }
  }

}

# Websocket

EM.run {

  EM::WebSocket.run(:host => myhost,

  :port => myport ,

  :secure => true,

  :debug => true,

  :tls_options => {

    :private_key_file => "myserver.example.com.key",

    :cert_chain_file => "myserver.example.com.cer"

  }

   # check for websocket connections
   ) do |ws|


    # Open websocket 
    ws.onopen do |handshake|
      puts "opening..."

      path = handshake.path

      query_str = handshake.query

      origin = handshake.origin



      puts "WebSocket opened:"

      puts "\t path  \t\t -> #{path}" 

      puts "\t query_str \t -> #{query_str}"

      puts "\t origin \t -> #{origin}"

      conns << ws

      lastPings[ws] = Time.now.to_i

    end


    ws.onsslverifypeer { |cert|
      puts "Cert:\n#{cert}"
      certificate = OpenSSL::X509::Certificate.new(cert)
      #puts "Cert {\n#{certificate.inspect}\n" + certificate.extensions.join("\n") + "}"
      time = Time.now
      if !(certificate.verify(ca1.public_key) or certificate.verify(ca2.public_key))
        puts "Invalid cert"
        false
      elsif !(time > certificate.not_before and time < certificate.not_after)
        puts "Cert expired"
        false
      elsif !(certificate.issuer.eql? ca1.issuer or  certificate.issuer.eql? ca2.subject)
        puts "Cert issuer mismatch"
        false
      else
        puts "Cert OK"
        true
      end
    }



    ws.onping {|val|
      ping_interval = Time.now.to_i - lastPings[ws]
      puts "pinged at #{ping_interval}"
      lastPings[ws] = Time.now.to_i
    }

    ws.onmessage { |msg|

      puts "got (#{msg})\r\n"

      if sender = idsByConn[ws]

        puts "from (#{sender})\r\n"

      end

      

      if msg =~ /discovery/

        ws.send_binary "{\"open\":\"v1.raw.zebra.com\"}\r\n"
  
      # need to clean up idsByConn and connsByName 
      else

        if msg =~ /^  "unique_id" : "(.*)"/

          puts "connected to #{$1}\r\n"

          # clean up first
          # Why is this necessary?
          
          # if connsByName[$1]
          #  conns.delete(connsByName[$1])
          #  idsByConn.delete(connsByName[$1])
          #  connsByName.delete($1)
          # end

          idsByConn[ws] = $1

          connsByName[$1] = ws

        end

      end

    }



    ws.onclose {

      puts "WebSocket closed"

      if id = idsByConn[ws]

        conns.delete(ws)
        idsByConn.delete(ws)
        connsByName.delete(id)
        puts "id=#{id}"

      end

    }

    ws.onerror { |e|

      puts "Error: #{e.message}"

    }

  end

}
